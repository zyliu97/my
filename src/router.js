import React from 'react'
import { Router, Route, Switch } from 'dva/router'
import IndexPage from './routes/IndexPage'
import Questions from './routes/Questions'
import Analysis from './routes/Analysis'
import List from './routes/List.js';


function RouterConfig ({history}) {
  return (
    <Router history={history}>
      <Switch>
        <Route path="/" exact component={IndexPage} />
        <Route path="/analysis" component={Analysis} />
        <Route path="/questions" component={Questions} />
        <Route path="/list" component={List} />
      </Switch>
    </Router>
  )
}

export default RouterConfig
