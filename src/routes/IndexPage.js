import React from 'react'
import { connect } from 'dva'
import styles from './IndexPage.css'
import { routerRedux } from 'dva/router'
import Header from '../components/Header'
import { Button } from 'antd'

function IndexPage ({dispatch}) {

  function pushQuestions () {
    dispatch(routerRedux.push({
      pathname: 'questions',
    }))

  }

  function pushAnalysis () {
    dispatch(routerRedux.push({
      pathname: 'analysis',
    }))

  }

  return (
    <div className={styles.normal}>
      <Header />
      <div className={styles.welcome} />
      <Button style={{fontSize: 20, marginRight: 20}}
        onClick={pushQuestions.bind(this)}>答题</Button>
      <Button style={{fontSize: 20}} onClick={pushAnalysis.bind(null, 'xxx')}>成语解析</Button>

    </div>
  )
}

IndexPage.propTypes = {}

export default connect()(IndexPage)
