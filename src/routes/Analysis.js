import React from 'react'
import { connect } from 'dva'
import AnalysisContent from '../components/Analysis/AnalysisContent'
import { routerRedux } from 'dva/router'

const Analysis = ({dispatch, analysis}) => {
  const setPage = (id) => {
    dispatch({
      type: 'analysis/setPage',
      payload: id,
    })
  }

  const pushTypes = () => {
    dispatch(routerRedux.push({
      pathname: 'list',
    }))

  }

  return (
    <div>
      <AnalysisContent onSetPage={setPage} onPushTypes={pushTypes}
        analysis={analysis} />
    </div>
  )
}

//connect方法就是把redux和组件联系在一起
//connect 方法传入的第一个参数是 mapStateToProps 函数，mapStateToProps 函数会返回一个对象，用于建立 State 到 Props 的映射关系。

// export default Products;
export default connect(({analysis}) => ({analysis}))
//({analysis:analysis}) =>({analysis:this.state.analysis})
//dispatch
(Analysis)
