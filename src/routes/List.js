import React from 'react'
import { connect } from 'dva'
import List from '../components/TypeList/List'
import Add from '../components/TypeList/Add'
import Header from '../components/Header'

const Lists = ({dispatch, lists, inputs}) => {
  function handleDelete (id) {
    dispatch({
      type: 'lists/delete',
      payload: id,
    })
  }

  function handleAdd () {
    dispatch({
      type: 'lists/add',
      payload: inputs.input,
    })
  }

  function handelChange (e) {
    dispatch({
      type: 'inputs/change',
      payload: e.target.value,
    })
  }

  return (
    <div>
      <Header />
      <Add onAdd={handleAdd} onChange={handelChange}  input={inputs.input} />
      <List onDelete={handleDelete} lists={lists} />
    </div>
  )
}

// export default Lists;
export default connect(({inputs, lists}) => ({
  inputs, lists,
}))(Lists)
