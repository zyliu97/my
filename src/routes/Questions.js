import React from 'react'
import { connect } from 'dva'
import AnswerQuestion from '../components/examination/AnswerQuestion'

import { routerRedux } from 'dva/router'

const Questions = ({dispatch, questions}) => {

  const handleSetName = (e) => {
    dispatch({
      type: 'questions/setName',
      payload: e
    })
  }

  const handleSetScore = (e) => {
    dispatch({
      type: 'questions/setScore',
      payload: e
    })
  }

  const handleSetSuggestion = (e) => {
    dispatch({
      type: 'questions/setSuggestion',
      payload: e
    })
  }

  const handleAppraiseShow = () => {
    dispatch({
      type: 'questions/appraiseShow'
    })
  }

  const pushLists = () => {
    dispatch(routerRedux.push({
      pathname: 'list',
    }))
  }

  return (
    <div>
      <AnswerQuestion onPushLists={pushLists} onHandleAppraiseShow={handleAppraiseShow}
        setScore={handleSetScore} setSuggestion={handleSetSuggestion} setName={handleSetName}
        questions={questions} />
    </div>
  )
}

//connect方法就是把redux和组件联系在一起
//connect 方法传入的第一个参数是 mapStateToProps 函数，mapStateToProps 函数会返回一个对象，用于建立 State 到 Props 的映射关系。

// export default Products;
export default connect(({questions}) => ({questions}))
//({products:products}) =>({products:this.state.products})
//dispatch
(Questions)
