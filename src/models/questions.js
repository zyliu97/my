export default {
  namespace: 'questions',
  state: {
    questions: [
      {
        id: '1',
        title: '一',
        question: '毛毛虫回到家，对爸爸说了一句话，爸爸即场晕倒，毛毛虫说了什么话？',
        options: [
          '要买伞',
          '要买水',
          '要买鞋',
          '要买纸',
        ],
        answer: '要买鞋',
        appraise: [
          {
            id: 1,
            name: '用户A',
            score: '20分',
            suggestion: '简练,快速了解其深意'
          },
        ]
      },

      {
        id: '2',
        title: '二',
        question: '有一个说法，一般来说，对于人们不必花力气打的东西是什么？',
        options: [
          '打棉花',
          '打哈欠',
          '打空气',
          '打拳击',
        ],
        answer: '打哈欠',
        appraise: [
          {
            id: 2,
            name: '用户B',
            score: '10分',
            suggestion: '内容精确，重点突出'
          }

        ]
      },

      {
        id: '3',
        title: '三',
        question: '有一个人们常见并且大部分人都认识的字，所有的人见了必定都会念错。这是什么字?',
        options: [
          '"对"字',
          '"好"字',
          '"坏"字',
          '"错"字',
        ],
        answer: '"错"字',
        appraise: [
          {
            id: 3,
            name: '用户C',
            score: '30分',
            suggestion: '提高做题的效率'
          }

        ]
      },
      {
        id: '4',
        title: '四',
        question: '有一个说法，一般来说，对于人们不必花力气打的东西是什么？',
        options: [
          '打棉花',
          '打哈欠',
          '打空气',
          '打拳击',
        ],
        answer: '打哈欠',
        appraise: [
          {
            id: 4,
            name: '用户D',
            score: '40分',
            suggestion: '利于简便学习'
          }
        ]
      },
      {
        id: '5',
        title: '五',
        question: '有一个人们常见并且大部分人都认识的字，所有的人见了必定都会念错。这是什么字?',
        options: [
          '"对"字',
          '"好"字',
          '"坏"字',
          '"错"字',
        ],
        answer: '"错"字',
        appraise: [
          {
            id: 5,
            name: '用户',
            score: '30分',
            suggestion: '内容精确，重点明确'
          }

        ]
      },
      {
        id: '6',
        title: '六',
        question: '毛毛虫回到家，对爸爸说了一句话，爸爸即场晕倒，毛毛虫说了什么话？',
        options: [
          '要买伞',
          '要买水',
          '要买鞋',
          '要买纸',
        ],
        answer: '要买鞋',
        appraise: [
          {
            id: 6,
            name: '用户D',
            score: '40分',
            suggestion: '适合大众'
          }
        ]
      },

    ],
    scores: [
      '10分',
      '30分',
      '50分',
      '70分',
      '90分',
      '100分'
    ],
    appraiseShow: false,
    name: '',
    score: this.scores,
    suggestion: '',
    problemDetail: this.questions,
  },

  reducers: {
    'appraiseShow' (state) {
      return {
        ...state,
        appraiseShow: !state.appraiseShow,
      }
    },

    setName (state, {payload: e}) {
      return {
        ...state,
        name: e.target.value
      }
    },
    setScore (state, {payload: e}) {
      let value = e.target.value
      return {
        ...state,
        score: value,
      }
    },
    setSuggestion (state, {payload: e}) {
      return {
        ...state,
        suggestion: e.target.value
      }
    }
  },
  effects: {}
}


