export default {
  namespace: 'lists',
  state: [
    {id: 1, problemTypes: 'english'},
    {id: 2, problemTypes: 'math'},
  ],
  reducers: {
    add (state, {payload: problemTypes}) {
      let id = state.reduce(
        (previous, current) => (previous.id > current.id ? previous : current),
      ).id
      id += 1
      return [...state, {problemTypes, id}]
    },
    delete (state, {payload: id}) {
      return state.filter(item => item.id !== id)
    },
  },
}
