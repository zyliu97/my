export default {
  namespace: 'inputs',
  state: {
    input: '',
  },
  reducers: {
    change(state, { payload: problemTypes}) {
      return {input: problemTypes};
    },
  },
};
