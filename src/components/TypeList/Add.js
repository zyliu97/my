import React from 'react'
import PropTypes from 'prop-types'
import { Input, Button } from 'antd'

const Add = ({onAdd, onChange, input}) => {
  return (
    <div>
      <br />
      <Input
        placeholder="Enter your problemTypes"
        style={{width: '20%', margin: '10px 20px'}}
        value={input}
        onChange={onChange}
      />
      <Button type="primary" onClick={onAdd}>添加</Button>
    </div>
  )
}

Add.propTypes = {
  onAdd: PropTypes.func.isRequired,
  input: PropTypes.string.isRequired,
}

export default Add
