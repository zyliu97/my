import React, { Component } from 'react'
import styles from './examination.css'
import { Button, Radio, notification } from 'antd'

const RadioGroup = Radio.Group

class QuestionItems extends Component {

  state = {
    result: '',
  }

  query = {
    result: {
      value: '',
    },
  }

  handleChange = (name, event) => {
    let newState = {}
    newState[name] = event.target.value
    this.setState(newState)
    for (let key in this.query) {
      if (key === name) {
        this.query[key].value = event.target.value
      }
    }
  }

  submit = () => {
    const {problemDetail} = this.props
    if (this.state.result === problemDetail.answer) {
      const args = {
        message: '您的答案是' + this.state.result + ',回答正确！',
        description: '',
        duration: 1
      }
      notification.open(args)
    }
    else if (this.state.result === '') {
      const args = {
        message: '请必须选择一项答案！',
        description: '',
        duration: 1
      }
      notification.open(args)

    }
    else {
      const args = {
        message: '您的答案是' + this.state.result + ' ,回答错误 ! 正确答案为：' + problemDetail.answer,
        description: '',
        duration: 2
      }
      notification.open(args)
    }
  }

  render () {
    const {problemDetail} = this.props
    return (
      <div className={styles.questions}>
        <h4>第{problemDetail.title}题</h4>
        <p className={styles.questionItems}>{problemDetail.question}</p>
        <br />
        <RadioGroup onChange={this.handleChange.bind(this, 'result')}>
          {problemDetail.options.length > 0 && problemDetail.options.map((item) =>
            <Radio key={item} type="radio" name="radios" value={item}>{item}</Radio>
          )}
        </RadioGroup>
        <Button style={{marginLeft: 100}} type="primary" onClick={this.submit}>提交</Button>
      </div>
    )
  }
}

export default QuestionItems



