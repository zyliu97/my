import React from 'react'
import { Form,message, Input } from 'antd'
import styles from './examination.css'

class Appraise extends React.Component {

  handleSubmit = (event) => {
    event.preventDefault()
    const {onAdd} = this.props
    const {name, score, suggestion} = this.props.questions
    const addContent = {
      name: name,
      score: score,
      suggestion: suggestion
    }
    onAdd(addContent)
  }

  info = () => {
    message.info('请注意信息是否填写完整！');
  };

  render () {
    const {questions, setName, setScore, setSuggestion} = this.props
    return (
      <div className={styles.appraiseContent}>
        <br />
        <p>💯分制</p>
        <div className={styles.appraiseOptions}>
          <div>
            <p>用户名：</p>
            <p>评分:</p>
            <p>建议:</p>
          </div>
          <div>
            <Form onSubmit={this.handleSubmit}>
              <p><Input type="text" onChange={setName} /></p>
              <p><select name="score" onChange={setScore} style={{width: '90%'}}>
                {questions.scores.length > 0 && questions.scores.map((item, index) =>
                  <option key={index} value={item}>{item}</option>)}
              </select></p>
              <p><Input onChange={setSuggestion} /></p>
              <button type='submit' value='submit' onClick={this.info} style={{marginTop: 10}}>提交</button>
            </Form>
          </div>
        </div>
      </div>
    )
  }
}

export default Appraise




