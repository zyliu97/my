import React, { Component } from 'react'
import QuestionItems from './QuestionItems'
import Header from '../Header'
import { Button, Rate, } from 'antd'
import styles from './examination.css'
import Appraise from './Appraise'

class AnswerQuestion extends Component {

  state = {
    problemDetail: this.props.questions.questions[0],
  }

  onDetail = (problemDetail) => {
    this.setState({problemDetail: problemDetail})
  }

  onAdd = (addContent) => {
    const {problemDetail} = this.state
    let newTests = problemDetail.appraise
    newTests.push(addContent)
    this.setState({tests: newTests})
  }

  render () {
    const {problemDetail} = this.state
    const {questions, onHandleAppraiseShow, setName, setSuggestion, setScore} = this.props
    return (
      <div style={{textAlign: 'center'}}>
        <Header />
        <div className={styles.normal}>
          <div className={styles.content}>
            <div className={styles.contentStyles}>
              <div>
                <div className={styles.title}>
                  {questions.questions.length > 0 && questions.questions.map((item) =>
                    <div className={styles.titleLocation} key={item.id}>
                      <Button onClick={() => {this.onDetail(item)}}
                        value={item.options}>第{item.title}题</Button>
                    </div>)}
                </div>
                <br />
                <hr />
                <QuestionItems problemDetail={problemDetail} />
                <div className={styles.toAppraise}>
                  <Button onClick={onHandleAppraiseShow}>我要评价</Button>
                </div>
                <div>
                  <p>评价：<Rate /></p>
                  {problemDetail.appraise.length > 0 && problemDetail.appraise.map((item) =>
                    <div key={item.id}><p
                      className={styles.options}>{item.name}：评分({item.score})</p>
                      <p className={styles.options}>建议：{item.suggestion}</p></div>)}
                </div>
              </div>
              {questions.appraiseShow &&
               <div><Appraise onAdd={this.onAdd} setName={setName} setSuggestion={setSuggestion}
                 setScore={setScore} questions={questions} /></div>}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default AnswerQuestion
