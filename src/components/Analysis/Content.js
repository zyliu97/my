import styles from './AnalysisContent.css'

const Content = ({title, analysis}) => {

  return (
    <div>
      <h3>{title}</h3>
      <p className={styles.analysis}>{analysis}</p>
    </div>
  )
}

export default Content
