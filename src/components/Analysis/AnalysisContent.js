import React, { Component } from 'react'
import Header from '../Header'
import Content from './Content'
import PageButton from './PageButton'
import styles from './AnalysisContent.css'
import { Button, Card } from 'antd'

class AnalysisContent extends Component {

  state = {
    indexList: [],//当前渲染的数据
    current: 1, //当前页码
    pageSize: 1, //每页显示的条数
    goValue: 0,  //条数index
    totalPage: 6,//总页数
    lists: [],
    data:null
  }

  componentWillMount () {
    //设置总页数
    this.setState({
      //ceil() 方法可对一个数进行上舍入
      totalPage: Math.ceil(this.props.analysis.analysis.length / this.state.pageSize),
    })
    this.pageNext(this.state.goValue)

  }

  //设置内容
  setPage = (num) => {
    this.setState({
      //slice() 方法可从已有的数组中返回选定的元素(arrayObject.slice(start,end))
      indexList: this.props.analysis.analysis.slice(num, num + this.state.pageSize)
    })
  }

  pageNext = (num) => {
    this.setPage(num)
  }

  getNet = () => {
    fetch('../../data.json')
      .then((res) => {
        console.log(res.status)
        return res.json()
      })
      .then((data) => { this.setState({lists: data.listData}) })
      .catch((e) => { console.log(e.message) })
  }

  componentDidMount () {
    this.getNet()
  }

  

    render () {
    const {onPushTypes} = this.props
    return (
      <div style={{textAlign: 'center'}}>
        <Header />
        <div className={styles.normal}>
          <div className={styles.content}>
            <div className={styles.contentStyles}>
              <div>
                {this.state.indexList.map(item => <Content {...item} />)}
              </div>
              <PageButton {...this.state} pageNext={this.pageNext} />
              <br />
              <Button className={styles.alterQuestionsTypes}
                onClick={onPushTypes.bind(this)}>修改题型</Button>
            </div>
          </div>
          <div>
            <Card title="资源导航" style={{width: 860, margin: '10px 0 0 0'}}>
              {
                this.state.lists.map((e) => {
                  return (
                    <p><a href={e.url} target="_blank">{e.title}</a></p>
                  )
                })
              }
            </Card>
          </div>
        </div>
      </div>
    )
  }
}

export default AnalysisContent
