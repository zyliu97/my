import React, { Component } from 'react'
import { Pagination } from 'antd'

class PageButton extends Component {

  state = {
    num: 0,
    pageNum: this.props.current
  }

  onChange (pageNum) {
    if (this.state.pageNum < this.props.totalPage) {
      this.setState({
        num: this.state.num + this.props.pageSize,
        pageNum: pageNum
      }, function () {
        this.props.pageNext(this.state.num)
      })
    }
    else if (this.state.pageNum > 1) {
      this.setState({
        num: this.state.num - this.props.pageSize,
        pageNum: pageNum
      }, function () {
        this.props.pageNext(this.state.num)
      })
    }
  }

  render () {
    const {pageNum} = this.state
    const {totalPage, pageSize} = this.props
    return (
      <div>
        <Pagination onChange={this.onChange.bind(this)} total={totalPage}
          current={pageNum} pageSize={pageSize} />
      </div>
    )
  }
}

export default PageButton
