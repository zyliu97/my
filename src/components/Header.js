import React, { Component } from 'react'
import { Button, Breadcrumb,Modal } from 'antd'
const confirm = Modal.confirm;

class Header extends Component {

  showConfirm =() => {
    confirm({
      title: '感谢您的注册！',
      content: '点确认 1 秒后关闭',
      onOk: () => {
        return new Promise((resolve) => {
          setTimeout(resolve, 1000);
        });
      },
      onCancel: function() {}
    });
  }

  render () {
    return (
      <div>
        <header style={{
          backgroundColor: '#000',
          paddingLeft: 20,
          height: 40,
          paddingTop: 10,
          marginBottom: 10
        }}>
          <div style={{display: 'flex', justifyContent: 'space-between', flexWrap: 'nowrap'}}>
            <Breadcrumb>
              <Breadcrumb.Item href="http://localhost:8000/#/">
                <strong style={{color: '#fff'}}>首页</strong>
              </Breadcrumb.Item>
              <Breadcrumb.Item href="http://localhost:8000/#/questions">
                <strong style={{color: '#fff'}}>答题</strong>
              </Breadcrumb.Item>
              <Breadcrumb.Item href="http://localhost:8000/#/analysis">
                <strong style={{color: '#fff'}}>成语解析</strong>
              </Breadcrumb.Item>
            </Breadcrumb>
          </div>
        </header>
        <div style={{
          display: 'flex',
          justifyContent: 'space-between',
          flexWrap: 'nowrap',
          backgroundColor: '#E6E6E6',
          height: 220,
          marginBottom: 10
        }}>
          <div style={{textAlign: 'left'}}>
            <p style={{fontSize: 40, marginLeft: 180, marginTop: 25}}>答题</p>
            <p style={{marginLeft: 180}}>高并发、高速度、强悍的远程代答平台、答题官方答题站。</p>
            <Button type="primary" onClick={this.showConfirm}  style={{marginLeft: 180}}>立即注册账号</Button>
          </div>
        </div>
      </div>
    )
  }
}

export default Header

