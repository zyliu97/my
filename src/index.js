import dva from 'dva'
import './index.css'

// 1. Initialize
const app = dva({
  initialState: {
  },
})

// 2. Plugins
// app.use({});

// 3. Model
app.model(require('./models/questions').default)
app.model(require('./models/analysis').default)
app.model(require('./models/TypeList/list').default);
app.model(require('./models/TypeList/add').default);


// 4. Router
app.router(require('./router').default)

// 5. Start
app.start('#root')
